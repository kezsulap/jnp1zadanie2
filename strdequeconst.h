#ifndef __STRDEQUECONST_H__
#define __STRDEQUECONST_H__

#ifdef __cplusplus
namespace jnp1 {
extern "C" {
#endif //__cplusplus

unsigned long emptystrdeque();

#ifdef __cplusplus
}
}
#endif //__cplusplus

#endif //__STRDEQUECONST_H__