#include "strdequeconst.h"
#include <unordered_map>

namespace jnp1 {
namespace {
	const unsigned long EMPTY_STR_DEQUE_ID = 0;
}

unsigned long emptystrdeque() {
	return EMPTY_STR_DEQUE_ID;
}
}