//#define NDEBUG
#include "strdeque.h"
#include "strdequeconst.h"
#include <cassert>
#include <cstddef>
#include <deque>
#include <utility>
#include <unordered_map>
#include <string>
#include <iostream>

#ifdef NDEBUG
const bool debug = false;
#else
const bool debug = true;
#endif
namespace jnp1 {
namespace {

using strdeque = std::deque<std::string>;
std::unordered_map<unsigned long, strdeque> & get_strdeque_map() {
	static std::unordered_map<unsigned long, strdeque> strdeque_map;
	return strdeque_map;
}

unsigned long next_id() {
	static unsigned long current_id = emptystrdeque() + 1;
	return current_id++;
}


}


unsigned long strdeque_new() {
	if (debug) {
		std::cerr << "strdeque_new()" << std::endl;
	}
	strdeque empty;
	unsigned long id = next_id();
	if (debug) {
		std::cerr << "\tcreate new queue nr: " << id << std::endl;
	}
	get_strdeque_map().insert(std::make_pair(id, empty));
	return id;
}

void strdeque_delete(unsigned long id) {
	if (debug) {
		std::cerr << "strdeque_delete(" << id << ")" << std::endl;
	}
	if(id == emptystrdeque()){
		if (debug) {
			std::cerr << "\tattempt to delete empty_queue" << std::endl;
		}
		return;
	}
	auto it = get_strdeque_map().find(id);
	if (it != get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "\tqueue deleted" << std::endl;
		}
		get_strdeque_map().erase(it);
	}
	else if (debug) {
		std::cerr << "\tno such queue found" << std::endl;
	}
	
}

size_t strdeque_size(unsigned long id) {
	if (debug) {
		std::cerr << "strdeque_size(" << id << ")" << std::endl;
	}
	if(id == emptystrdeque()){
		if (debug) {
			std::cerr << "\tget size of empty queue" << std::endl;
		}
		return 0;
	}
	auto it = get_strdeque_map().find(id);
	if (it != get_strdeque_map().end()) {
		size_t answer = it->second.size();
		if (debug) {
			std::cerr << "\tsize = " << answer << std::endl;
		}
		return answer;
	}
	else {
		if (debug) {
			std::cerr << "\tno such queue found" << std::endl;
		}
		return 0;
	}
}

void strdeque_insert_at(unsigned long id, size_t pos, const char* value) {
	if (value == NULL) {
		if (debug) {
			std::cerr << "attemp to insert NULL" << std::endl;
		}
		return;
	}
	if (debug) {
		std::cerr << "strdeque_insert_at(" << id << ", " << pos << ", " <<
		std::string(value) << ")" <<  std::endl;
	}
	if(id == emptystrdeque()){
		if (debug) {
			std::cerr << "\ttry to insert to empty queue" << std::endl;
		}
		return;
	}
	auto strdeque_it = get_strdeque_map().find(id);
	if (strdeque_it == get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "no such spot" << std::endl;
		}
		return;
	}
	strdeque & current_strdeque = strdeque_it->second;
	size_t size = current_strdeque.size();
	if (pos > size)
		pos = size;
	auto it = current_strdeque.begin() + pos;
	std::string str(value);
	current_strdeque.insert(it, str);
	if (debug) {
		std::cerr << "insert done" << std::endl;
	}
}


void strdeque_remove_at(unsigned long id, size_t pos) {
	if (debug) {
		std::cerr << "strdeque_remove_at(" << id << ", " << pos << ")" << std::endl;
	}
	if(id == emptystrdeque()){
		if (debug) {
			std::cerr << "\tattempt to remove from empty queue" << std::endl;
		}
		return;
	}
	auto strdeque_it = get_strdeque_map().find(id);
	if(strdeque_it == get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "\tno such queue found" << std::endl;
		}
		return;
	}
	strdeque & current_strdeque = strdeque_it->second;
	size_t size = current_strdeque.size();
	if(pos >= size) {
		if (debug) {
			std::cerr << "\tindex out of bounds" << std::endl;
		}
		return;
	}
	auto it = current_strdeque.begin() + pos;
	current_strdeque.erase(it);
	if (debug) {
		std::cerr << "\tdone" << std::endl;
	}
}

const char* strdeque_get_at(unsigned long id, size_t pos) {
	if (debug) {
		std::cerr << "strdeque_get_at(" << id << ", " << pos << ")" << std::endl;
	}
	if(id == emptystrdeque()){
		if (debug) {
			std::cerr << "\tattemp to get from empty queue" << std::endl;
		}
		return NULL;
	}
	auto strdeque_it = get_strdeque_map().find(id);
	if (strdeque_it == get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "\tno such queue found" << std::endl;
		}
		return NULL;
	}
	strdeque & current_strdeque = strdeque_it->second;
	size_t size = current_strdeque.size();
	if(debug) {
		std::cerr << "\tsize = " << size << std::endl;
	}
	if(pos >= size) {
		if (debug) {
			std::cerr << "\tindex out of bounds" << std::endl;
		}
		return NULL;
	}
	auto it = current_strdeque.begin() + pos;
	if (debug) {
		std::cerr << "\tdone" << std::endl;
	}
	return it->c_str();
}

void strdeque_clear(unsigned long id) {
	if (debug) {
		std::cerr << "strdeque_clear(" << id << ")" << std::endl;
	}
	if(id == emptystrdeque()){
		if (debug) {
			std::cerr << "\tattempt to clear empty queue" << std::endl;
		}
		return;
	}
	auto strdeque_it = get_strdeque_map().find(id);
	if (strdeque_it == get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "\tno such queue found" << std::endl;
		}
		return;
	}
	strdeque & current_strdeque = strdeque_it -> second;
	current_strdeque.clear();
	if (debug) {
		std::cerr << "\tdone" << std::endl;
	}
}

int strdeque_comp(unsigned long id1, unsigned long id2) {
	if (debug) {
		std::cerr << "strdeque_comp(" << id1 << ", " << id2 << ")" << std::endl;
	}
	auto strdeque_it1 = get_strdeque_map().find(id1);
	auto strdeque_it2 = get_strdeque_map().find(id2);
	// Wiem że jest brzydko w cholere, ale nie miałem juz czasu sie bawic - przynajmniej działa
	strdeque strdeque1, strdeque2;
	if(strdeque_it1 == get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "\tfirst queue is empty/doesn't exist" << std::endl;
		}
		strdeque1 = strdeque();
	}
	else {
		if (debug) {
			std::cerr << "\tfirst queue is not empty" << std::endl;
		}
		strdeque1 = strdeque_it1 -> second;
	}

	if(strdeque_it2 == get_strdeque_map().end()) {
		if (debug) {
			std::cerr << "\tsecond queue is empty/doesn't exist" << std::endl;
		}
		strdeque2 = strdeque();
	}
	else {
		if (debug) {
			std::cerr << "\tsecond queue is not empty" << std::endl;
		}
		strdeque2 = strdeque_it2 -> second;
	}

	if (strdeque1 > strdeque2) {
		if (debug) {
			std::cerr << "\tfirst queue is greater" << std::endl;
		}
		return 1;
	}
	else if (strdeque1 < strdeque2) {
		if (debug) {
			std::cerr << "\tsecond queue is greater" << std::endl;
		}
		return -1;
	}
	else {
		if (debug) {
			std::cerr << "\tqueues are equal" << std::endl;
		}
		return 0;
	}
}

}